var entity = require('node-entity');

var Star = entity.extend(function(starOne, starTwo, starThree, starFour, starFive) {
    this.starOne = starOne;
    this.starTwo = starTwo;
    this.starThree = starThree;
    this.starFour = starFour;
    this.starFive = starFive;
});

module.exports.Star = Star;