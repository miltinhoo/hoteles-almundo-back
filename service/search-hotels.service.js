var star = require('../entity/star.entity')
var constants = require('./constants.service')

const constantsStars = new constants.StarConstant();

var searchHotels = function(page, name, stars) {
    let hotels = hoteles;
    if (name) {
        hotels = searchHotelsByName(name, hotels);
    }
    if (stars) {
        hotels = searchHotelsByStars(stars, hotels);
    }
    if (hotels) {
        const numHotels = hotels.length;
        const hotelsPerPage = searchHotelsPerPage(page, hotels);
        return responseHotels(hotelsPerPage, numHotels);
    }
    return responseHotels([]);
}

var searchHotelsByName = function(name, hotels) {
    let myReg = new RegExp(name.toLowerCase() + ".*")
    return hotels.filter(_ => _.name.toLowerCase().match(myReg));
}

var searchHotelsByStars = function(stars, hotels) {
    let hotelsFiter = new Array();
    let hotelFilterResponse = new Array();

    if (stars.starOne !== 'true' && stars.starTwo !== 'true' && stars.starThree !== 'true' &&
        stars.starFour !== 'true' && stars.starFive !== 'true') {
        return hotels;
    }

    if (stars.starOne === 'true') {
        const hotelsWithFilter = hotels.filter(_ => _.stars === constantsStars.starOneValue);
        if (hotelsWithFilter && hotelsWithFilter.length > 0) {
            hotelsFiter.push(hotelsWithFilter);
        }
    }
    if (stars.starTwo === 'true') {
        const hotelsWithFilter = hotels.filter(_ => _.stars === constantsStars.starTwoValue);
        if (hotelsWithFilter && hotelsWithFilter.length > 0) {
            hotelsFiter.push(hotelsWithFilter);
        }
    }
    if (stars.starThree === 'true') {
        const hotelsWithFilter = hotels.filter(_ => _.stars === constantsStars.starThreeValue);
        if (hotelsWithFilter && hotelsWithFilter.length > 0) {
            hotelsFiter.push(hotelsWithFilter);
        }
    }
    if (stars.starFour === 'true') {
        const hotelsWithFilter = hotels.filter(_ => _.stars === constantsStars.starFourValue);
        if (hotelsWithFilter && hotelsWithFilter.length > 0) {
            hotelsFiter.push(hotelsWithFilter);
        }
    }
    if (stars.starFive === 'true') {
        const hotelsWithFilter = hotels.filter(_ => _.stars === constantsStars.starFiveValue);
        if (hotelsWithFilter && hotelsWithFilter.length > 0) {
            hotelsFiter.push(hotelsWithFilter);
        }
    }

    if (hotelsFiter) {
        hotelsFiter.forEach(_ => {
            _.forEach(x => hotelFilterResponse.push(x));
        });
    }

    return hotelFilterResponse;
}

var searchHotelsPerPage = function(page, hotels) {
    let pageFrom = parseInt(page);

    if (pageFrom > 1) {
        pageFrom = pageFrom * hotelsPerPage() - (hotelsPerPage() - 1);
    }

    let pageUntil = pageFrom + hotelsPerPage() - 1;
    return hotels.slice(pageFrom - 1, pageUntil);
}

var hotelsPerPage = function() {
    return 10;
}

var totalHotels = function(numberHotels) {
    let totalNumber = {
        totalHotelsNumber: Math.ceil(numberHotels / hotelsPerPage())
    };
    return response(totalNumber);
}

var responseHotels = function(hotelsList, hotelsTotalNumber) {
    let hotelsObject = {
        hotelsTotalNumber: hotelsTotalNumber,
        hotels: hotelsList
    }
    return response(hotelsObject);
}

var response = function(response) {
    return {
        response
    };
}

var hoteles = require('./mock/data')

module.exports.searchHotels = searchHotels;
module.exports.totalHotels = totalHotels;