var entity = require('node-entity');

const StarConstant = entity.extend(function() {
    this.starOneValue = 1;
    this.starTwoValue = 2;
    this.starThreeValue = 3;
    this.starFourValue = 4;
    this.starFiveValue = 5;
});

module.exports.StarConstant = StarConstant;