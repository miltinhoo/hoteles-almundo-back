var star = require('../entity/star.entity')

var mapperStars = function(starOne, starTwo, starThree, starFour, starFive) {
    return new star.Star(starOne, starTwo, starThree, starFour, starFive);
}

module.exports.mapperStars = mapperStars;