var getStatusHotelsResponse = function(response) {
    if (response.response.hotels.length === 0) {
        return 404;
    } else {
        return 200;
    }
}

module.exports.getStatusHotelsResponse = getStatusHotelsResponse;