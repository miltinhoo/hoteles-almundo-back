var express = require('express')
var cors = require('cors')
var http = require('http')
var app = express()
var users = ['oscar', 'juan', 'marcos']
var searchHotelsService = require('./service/search-hotels.service')
var mapperStars = require('./service/mapper-stars.service')
var statusManager = require('./service/statusManager.service')

var originsWhitelist = [
    'http://localhost:4200',
    'http://www.almundo.com'
];
var corsOptions = {
    origin: function(origin, callback) {
        var isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
        callback(null, isWhitelisted);
    },
    credentials: true
}
app.use(cors(corsOptions));

app.get('/', (req, res) => {
    res.status(200).send("Welcome to API REST")
})

app.get('/hotels/:page', (req, res) => {
    let stars = mapperStars.mapperStars(req.query.starOne, req.query.starTwo,
        req.query.starThree, req.query.starFour, req.query.starFive);

    let response = searchHotelsService.searchHotels(req.params.page, req.query.hotelName, stars);
    res.status(statusManager.getStatusHotelsResponse(response)).send(response);
})

app.get('/hotels/numberHotels/:numberHotels', (req, res) => {
    let totalHotels = searchHotelsService.totalHotels(req.params.numberHotels);
    res.status(200).send(totalHotels);
})

http.createServer(app).listen(8001, () => {
    console.log('Server started at http://localhost:8001');
});